module Basics

-- |Exercise: 1 star (nandb)
total nandb : Bool -> Bool -> Bool
nandb False _ = True
nandb _ False = True
nandb _ _ = False

-- |Exercise: 1 star (andb3)
total andb3 : Bool -> Bool -> Bool -> Bool
andb3 True True True = True
andb3 _ _ _ = False

-- |Exercise: 1 star (factorial)
total factorial : Nat -> Nat
factorial Z = 1
factorial (S k) = S k * factorial k

-- |Exercise: 2 stars (blt_nat)
blt_nat : Nat -> Nat -> Bool
blt_nat Z Z = False
blt_nat Z _ = True
blt_nat (S n) (S m) = blt_nat n m

-- |Exercise: 1 star (plus_id_exercise)
plus_id_exercise : {n : Nat} -> {m : Nat} -> {o : Nat} -> (n = m) -> (m = o) -> (n + m = m + o)
plus_id_exercise p1 p2 = ?pie

-- |Exercise: 2 stars (mult_S_1)
mult_S_1 : m = S n -> m * (1 + n) = m * m
mult_S_1 p = ?ms1

-- |Exercise: 1 star (zero_nbeq_plus_1)
zero_nbeq_plus_1 : (m : Nat) -> (S n == Z) = False
zero_nbeq_plus_1 = ?znp1

-- |Exercise: 2 stars (boolean functions)
identityFnAppliedTwice : (f : Bool -> Bool) -> ((x : Bool) -> f x = x) -> ((b : Bool) -> f (f b) = b)
identityFnAppliedTwice f p = ?ifat

-- |Alternative form (?)
negationFnAppliedTwice : (f : Bool -> Bool) -> (f x = not x) -> (f (f y) = y)
negationFnAppliedTwice f p = ?nfat

-- |Exercise: 2 stars (andb_eq_orb)
andb_eq_orb : (b : Bool) -> (c : Bool) -> (b && c = b || c) -> b = c
andb_eq_orb b c p = ?aeo

-- |Exercise: 3 stars (binary)
-- (a) Datatype definition
data Bin : Type where
    Zero : Bin
    Twice : Bin -> Bin
    TwicePlus : Bin -> Bin

-- (b) successor and conversion
total succ : Bin -> Bin
succ Zero = TwicePlus Zero
succ (Twice x) = TwicePlus x
succ (TwicePlus x) = Twice (succ x)

total binToNat : Bin -> Nat
binToNat Zero = Z
binToNat (Twice x) = binToNat x + binToNat x
binToNat (TwicePlus x) = S (binToNat x + binToNat x)

-- (c) conversion test
total binTest : (b : Bin) -> binToNat (succ b) = S (binToNat b)
binTest Zero = refl
binTest (Twice x) = refl
binTest (TwicePlus x) = let ind = binTest x in ?p2

-- |Exercise: 2 stars, optional (decreasing)
-- |Undone
total decreasing : Nat -> Nat -> Nat
decreasing Z _ = Z
decreasing (S n) k = decreasing n (S k)

---------- Proofs ----------

Basics.p2 = proof
  intros
  compute
  rewrite ind
  rewrite sym ind
  compute
  rewrite plusSuccRightSucc (binToNat x) (binToNat x)
  trivial


Basics.aeo = proof
  compute
  intro b,c
  compute
  case b
  compute
  intro
  trivial
  compute
  intro
  rewrite (sym p)
  trivial


Basics.ifat = proof
  intros
  rewrite sym (p b)
  rewrite sym (p b)
  trivial


Basics.znp1 = proof
  intros
  trivial


Basics.ms1 = proof
  intros
  rewrite p
  trivial


Basics.pie = proof
  intros
  rewrite sym p1
  rewrite p2
  trivial



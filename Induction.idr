module Induction

-- |Exercise: 2 stars (andb_true_elim2)
-- TODO
andb_true_elim2 : b && c = True -> c = True
--andb_true_elim2 {b = False} {c = False} impossible
--andb_true_elim2 {b = True} = Refl

-- |Exercise: 2 stars (basic_induction)
mult_0_r : (n : Nat) -> n * 0 = 0

-- |Exercise: 2 stars (double_plus)
double : Nat -> Nat
double Z = Z
double (S n) = S (S (double n))

double_plus : double n = n + n
double_plus = ?dp

-- |Exercise: 1 star (destruct_induction)
-- Briefly explain the difference between the tactics destruct and induction.
-- TODO

-- |Exercise: 4 stars (mult_comm)
plus_swap : (n : Nat) -> (m : Nat) -> (p : Nat) -> n + (m + p) = m + (n + p)

mult_comm : (n : Nat) -> (m : Nat) -> m * n = n * m

---------- Proofs ----------
Induction.dp = proof
  intro
  induction n
  trivial
  intros
  compute
  rewrite sym ihn__0
  rewrite plusSuccRightSucc n__0 n__0
  trivial



Induction.mult_0_r = proof
  intro
  induction n
  trivial
  intros
  compute
  trivial


---------- Proofs ----------

Induction.mult_comm = proof
  intro
  induction n
  intro
  compute
  rewrite sym (mult_0_r m)
  trivial
  intros
  compute
  induction m
  compute
  rewrite sym (mult_0_r n__0)
  trivial
  intros
  compute
  rewrite ihn__0 (S n__1)
  rewrite sym ihn__1
  rewrite plus_swap n__1 n__0 (mult n__0 n__1)
  rewrite (ihn__0 n__1)
  trivial


Induction.plus_swap = proof
  intro
  induction n
  compute
  intros
  trivial
  intros
  compute
  rewrite sym (ihn__0 m p)
  induction m
  compute
  trivial
  intros
  compute
  rewrite sym ihn__1
  trivial



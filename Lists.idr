module Lists

%default total

swap_pair : (a, b) -> (b, a)
swap_pair (x, y) = (y, x)

||| Exercise: 1 star (snd_fst_is_swap)
snd_fst_is_swap : (p : (a, b)) -> (snd p, fst p) = swap_pair p
snd_fst_is_swap = ?sfis

||| Exercise: 1 star, optional (fst_swap_is_snd)
fst_swap_is_snd : (p : (a, b)) -> fst (swap_pair p) = snd p
fst_swap_is_snd = ?fsis

||| Exercise: 2 stars (list_funs)
nonzeros : List Nat -> List Nat
nonzeros [] = []
nonzeros (Z :: xs) = nonzeros xs
nonzeros (x :: xs) = x :: nonzeros xs

||| Exercise: 3 stars, advanced (alternate)
alternate : List a -> List a -> List a
alternate [] ys = ys
alternate xs [] = xs
alternate (x :: xs) (y :: ys) = x :: y :: alternate xs ys

{-
alternateFail : List a -> List a -> List a
alternateFail [] ys = ys
alternateFail (x :: xs) ys = x :: alternateFail ys xs
-}

|||Exercise: 3 stars (bag_functions)
Bag : Type
Bag = List Nat

count : Nat -> Bag -> Nat
count _ [] = 0
count v (x :: xs) = match v x where
    match Z Z = 1 + count v xs
    match Z _ = count v xs
    match _ Z = count v xs
    match (S a) (S b) = match a b
    
countZero : (n : Nat) -> count n [] = 0
countZero n = Refl

add : Nat -> Bag -> Bag
add = (::)
--The rest is skipped for being trivial

|||Exercise: 3 stars, optional (bag_more_functions)

--Skipped for being trivial

|||TODO: Exercise: 3 stars (bag_theorem)
bag_theorem : (b : Bag) -> (n : Nat) -> S (count n b) = count n (add n b)
bag_theorem b n = ?bt

|||Exercise: 3 stars (list_exercises)
app_nil_end : (l : List a) -> l ++ [] = l
app_nil_end [] = Refl
app_nil_end (x :: xs) = cong (app_nil_end xs)

rev_involutive : (l : List a) -> reverse (reverse l) = l
rev_involutive = ?ri

---------- Proofs ----------

Lists.fsis = proof
  intros
  case p
  intros
  compute
  trivial


Lists.sfis = proof
  intros
  case p
  intros
  compute
  trivial


